#!/usr/bin/env bash

echo "~## Update & Upgrading ##~"
apt update -qq && apt upgrade -qqy

echo "~## Installing base packages ##~"
apt install -qqy gnupg2 curl ca-certificates zip unzip git python3 python3-pip software-properties-common \
ubuntu-keyring wget

echo "~## Setup nodejs ##~"
curl -sL https://deb.nodesource.com/setup_$NODE_VERSION.x | bash -

echo "~## Installing yarn ##~"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list

echo "~## Setup PHP Repo ##~"
add-apt-repository ppa:ondrej/php

echo "~## Setup nginx ##~"
curl https://nginx.org/keys/nginx_signing.key | gpg --dearmor | tee /usr/share/keyrings/nginx-archive-keyring.gpg >/dev/null
gpg --dry-run --quiet --import --import-options import-show /usr/share/keyrings/nginx-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] http://nginx.org/packages/ubuntu `lsb_release -cs` nginx" | tee /etc/apt/sources.list.d/nginx.list
echo -e "Package: *\nPin: origin nginx.org\nPin: release o=nginx\nPin-Priority: 900\n" | tee /etc/apt/preferences.d/99nginx

echo "~## Update ##~"
apt update -qq

echo "~## Install remaining packages ##~"
apt install -qqy php8.1-cli php8.1-dev php8.1-gd php8.1-curl php8.1-mysql php8.1-mbstring php8.1-xml \
 php8.1-zip php8.1-bcmath php8.1-redis php8.1-fpm php8.1-intl \
 nodejs yarn mysql-client nginx supervisor

echo "~## Fix issue with PEAR ##~"
curl -O https://pear.php.net/go-pear.phar
php go-pear.phar

echo "~## Install pcov ##~"
pecl install pcov

echo "~## Install swoole ##~"
pecl install openswoole

echo "~## Install awscli ##~"
pip3 install awscli

echo "~## Install composer ##~"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

echo "~## Install infection ##~"
wget https://github.com/infection/infection/releases/download/0.26.0/infection.phar
chmod +x infection.phar
mv infection.phar /usr/local/bin/infection

echo "~## Copy base ini into php folder ##~"
cp /root/90-base.ini /etc/php/8.1/cli/conf.d/

echo "~## Clean up ##~"
apt autoremove -qq
apt clean -qq
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
