# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## 1.3.0
### Added
- Infection testing package
### Fixed
- Missing php intl extension

## 1.2.0
### Added
- Swoole extension

## 1.1.0
### Changed
- Updated to PHP 8.1


## 1.0.0
Initial Version
